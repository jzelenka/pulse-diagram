#!/usr/bin/env python3
from matplotlib.backends.backend_qt5agg import\
        FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
from PyQt5 import QtGui
from PyQt5 import QtWidgets
from PyQt5 import QtCore
import itertools
import numpy as np
import sys


def main(pathprefix, files, styles, outname):
    freqsec = 1 
    fms = freqsec * 1000
    app = QtWidgets.QApplication(sys.argv)
    nrows = 6
    graph = Figure(figsize=(12, 6), dpi=300, facecolor="None")
    canvas = FigureCanvas(graph)
    canvas.setStyleSheet("background-color:transparent;")
    canvas.setAutoFillBackground(False)

    xcut = fms / 2
    times = [(-fms*0.01, xcut), (fms * 0.9, xcut + fms),
             (fms * 1.9, fms * 2.01)]
    titles = ["", "Quadrupole\nBender", "Helium Pulses",
              "IR Laser\nShutter", "Trap Exit\nElectrode", "Summary"]
    graph_specs = {"width_ratios": [i[1]-i[0] for i in times],
                   "height_ratios": [1] * nrows}
    spectra = graph.subplots(nrows, 3, sharey=True,
                             subplot_kw={"facecolor": (1, 1, 1, 0)},
                             gridspec_kw=graph_specs)
    # edited by hand based on content
    graph.subplots_adjust(wspace=0.025, right=0.99, top=1.02)

    # stolen from:
    # https://matplotlib.org/3.3.3/gallery/subplots_axes_and_figures/broken_axis.html
    d = 8  # proportion of vertical to horizontal extent of the slanted line
    kwargs = dict(marker=[(-1, -d), (1, d)], markersize=12, linestyle=None,
                  color='k', mec='k', mew=1, clip_on=False)

    [ax.remove() for ax in spectra[0, :]]
    topax = graph.add_subplot(nrows * 100 + 11, facecolor=(1, 1, 1, 0),
                              frame_on=False)
    topax.yaxis.set_visible(False)
    topax.xaxis.set_visible(False)
    topax.axis("equal")

    for i, j in itertools.product(range(1, nrows), range(3)):
        spectra[i, j].set_xlim(times[j])
        spectra[i, j].set_ylim(-0.1, 1.1)
        spectra[i, j].set_yticks([])
        for k in ['top', 'left', 'right']:
            spectra[i, j].spines[k].set_visible(False)
        if j:
            spectra[i, j].plot(
                [0, 0], [0, 0], transform=spectra[i, j].transAxes, **kwargs)
        if j-2:
            spectra[i, j].plot(
                 [1, 1], [0, 0], transform=spectra[i, j].transAxes, **kwargs)
        if j == 0:
            spectra[i, j].annotate(
                titles[i], xy=(0, 0.5), xytext=(
                    -spectra[i, j].yaxis.labelpad, 0),
                xycoords=spectra[i, j].yaxis.label, textcoords='offset points',
                size='large', ha='right', va='center', style="italic")
            spectra[i, j].set_xticks([0])
            spectra[i, j].set_xticklabels([""])
        if j == 1:
            spectra[i, j].plot([fms, fms],  [0,  1],
                               linestyle="--", color="lightgray")
            spectra[i, j].set_xticks([fms])
            spectra[i, j].set_xticklabels([""])
        if j == 2:
            spectra[i, j].set_xticks([fms*2])
            spectra[i, j].set_xticklabels([""])

    spectra[-1, 0].set_xticklabels(["0"])
    spectra[-1, 1].set_xticklabels(["N"])
    spectra[-1, 2].set_xticklabels(["2N"])
    spectra[-1, 1].set_xlabel('$\it{time}$ / s →', x=0.2)

    ang = np.linspace(np.pi, np.pi/2, 100)
    leng = 20
    halfx = np.concatenate((np.cos(ang),
                               np.cos(np.flip(ang+np.pi)) + leng))
    halfy = np.concatenate((np.sin(ang), np.sin(np.flip(ang+np.pi)) + 2))
    fullx = np.concatenate((halfx, halfx + leng + 2))
    fully = np.concatenate((halfy, np.flip(halfy)))
    doublx = np.concatenate((fullx, fullx + ((leng + 2) * 2)))
    doubly = np.tile(fully, 2)
    topax.plot(doublx, doubly - 1, color='gray', linewidth=1)
    topax.annotate("$N_i\ cycle$", (leng + 2, 3), ha="center", va="top")
    topax.annotate("$N_{i0}\ cycle$", ((leng + 2) * 3, 3),
                   ha="center", va="top")
    topax.set_ylim(0, 3.5)
    topax.set_xlim(-1.5, (leng + 2) * 4 - 0.5)

    qpbx = np.asarray([0, 1, 1, 201, 201, 1000])
    qpb = [np.append(qpbx,  qpbx+fms),  np.tile([0, 0, 1, 1, 0, 0], 2)]
    for ax in [spectra[1, i] for i in range(3)]:
        ax.plot(*qpb, color='black')
        ax.fill_between(*qpb, alpha=0.2, color='black')

    pulses_timing = np.concatenate((
        [0], np.tile([0.5, 20], 3), np.tile([0.2, 20], 5), [0.5]))
    pulses_timing_cum = np.repeat(np.cumsum(pulses_timing), 2)
    he_doublex = np.append(pulses_timing_cum,  pulses_timing_cum+fms)
    helium_pulses = [np.append(he_doublex, [fms*2]),
                     np.append(np.tile([0, 1, 1, 0], 18), [0])]
    for ax in [spectra[2, i] for i in range(3)]:
        ax.plot(*helium_pulses)

    shutter = [[0, 0, 1000, 1000, 2000], [0, 1, 1, 0, 0]]
    for ax in [spectra[3, i] for i in range(3)]:
        ax.plot(*shutter, color='orange')
        ax.fill_between(*shutter, alpha=0.2, color='orange')

    exx = np.asarray([0, 980, 980, 990, 990, 1000])
    ex = [np.append(exx, exx + 1000), np.tile([1, 1, 0, 0, 1, 1], 2)]
    for ax in [spectra[4, i] for i in range(3)]:
        ax.plot(*ex, color='red')

    reduced = np.asarray(shutter[1]) * 0.8
    reducedd = np.tile(reduced, 2)
    for ax in [spectra[5, i] for i in range(3)]:
        ax.plot(shutter[0], reduced, color='orange')
        ax.fill_between(shutter[0], reduced, alpha=0.2, color='orange')
        ax.plot(ex[0], ex[1] * 0.9 + 0.1, color='red')
        ax.plot(qpb[0], qpb[1] * 0.7, color='black')
        ax.fill_between(qpb[0], qpb[1] * 0.7, alpha=0.2, color='black')

    nicearrow = dict(facecolor="black", shrink=0.1, width=0.1, headwidth=3,
                     headlength=5)

    spectra[1, 0].annotate("$Trap$\n$Filling$", (201, 0.5), xytext=(300, 0.5),
                           ha="left", va="center", arrowprops=nicearrow)
    spectra[1, 1].annotate("$Trap$\n$Filling$", (1201, 0.5),
                           xytext=(1300, 0.5), ha="left", va="center",
                           arrowprops=nicearrow)
    spectra[2, 0].annotate("200 μs and 500 μs pulses,\n20 ms delays*", xy=(200, 0.5),
                           ha="left", va="center", style="italic")
    spectra[2, 1].annotate("repeat", xy=(1200, 0.5),
                           ha="left", va="center", style="italic")
    spectra[3, 0].annotate("$Shutter$\n$Open$", (250, 0.5), ha="left",
                           va="center")
    spectra[3, 1].annotate("$Shutter$\n$Closed$", (1250, 0.5), ha="left",
                           va="center")
    spectra[4, 1].annotate("$Ion\ Extraction$", (980, 0.5),
                           xytext=(880, 0.5), ha="right", va="center",
                           arrowprops=nicearrow)
    spectra[4, 2].annotate("$Ion\ Extraction$", (1980, 0.5),
                           xytext=(1880, 0.5), ha="right", va="center",
                           arrowprops=nicearrow)
    spectra[5, 0].annotate("$Fill$", (100, 0.4), ha="center", va="center")
    spectra[5, 1].annotate("$Fill$", (1100, 0.4), ha="center", va="center")
    spectra[5, 0].annotate("$Irradiate$", (300, 0.4), ha="left", va="center")
    spectra[5, 1].annotate("$Detect$", (990, 1.05), xytext=(1050, 1.6),
                           ha="left", va="center", arrowprops=dict(
        facecolor="black", shrink=0.05, width=0.1, headwidth=3, headlength=5))
    spectra[5, 2].annotate("$Detect$", (1980, 0.5), xytext=(1880, 0.5),
                           ha="right", va="center", arrowprops=nicearrow)

    main_window = QtWidgets.QMainWindow(windowTitle="Pulses viewer")

    graph.savefig(outname + ".jpg", dpi=300)
    graph.savefig(outname + ".png", dpi=300)
    pixmap = QtGui.QPixmap(outname + ".png")
    label = QtWidgets.QLabel(alignment=QtCore.Qt.AlignCenter)
    main_window.setCentralWidget(label)
    main_window.resizeEvent = lambda x: label.setPixmap(
            pixmap.scaled(x.size(), QtCore.Qt.KeepAspectRatio,
                          QtCore.Qt.SmoothTransformation))
    main_window.show()
    sys.exit(app.exec_())
    return


if __name__ == "__main__":
    pathprefix = './'
    files, styles = [], []
    outname = '2020_12_07_1s_mz229_seq'
    main(pathprefix, files, styles, outname)
